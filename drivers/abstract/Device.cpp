// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Device.h"

Device *Device::getParent() {
  return this->parent;
}

bool Device::setParent(Device *newParent) {
  this->parent = newParent;
  return true;
}

Device *Device::getNextSibling() {
  return this->nextSibling;
}

bool Device::setNextSibling(Device *newSibling) {
  this->nextSibling = newSibling;
  return true;
}

Device *Device::getFirstChild() {
  return this->firstChild;
}

char *Device::getGeneralType() {
  return &this->generalType[0];
}

char *Device::getSpecificType() {
  return &this->specificType[0];
}

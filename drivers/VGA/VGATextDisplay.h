// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "VGADisplay.h"
#include <unicode/Font.h>

#ifndef VGATEXTDISPLAY_H
#define VGATEXTDISPLAY_H

typedef enum Color {
  black = 0x0,
  blue = 0x1,
  green = 0x2,
  cyan = 0x3,
  red = 0x4,
  magenta = 0x5,
  brown = 0x6,
  grey = 0x7,
  darkGrey = 0x8,
  brightBlue = 0x9,
  brightGreen = 0xA,
  brightCyan = 0xB,
  brightRed = 0xC,
  brightMagenta = 0xD,
  yellow = 0xE,
  white = 0xF,
} Color;

typedef enum CursorMode {
  disabled,
  line,
  block,
} Mode;

/**
 * A text-mode 80x25 VGA display.
 *
 * Supports bitmap fonts, foreground/background colors, the hardware cursor, scrolling, etc.
 */
class VGATextDisplay : public VGADisplay {
private:
  IORange range;
  uint16_t cursor;
  CursorMode cursorMode;
  Unicode::Font *font;
  int width;
  int height;
  char *framebuffer = (char *) 0xB8000;
  void updateCursor();
  void scroll();
  void printGlyphs(char *str, Color fgColor, Color bgColor);
protected:
  char generalType[16] = "VGATextDisplay";
  char specificType[16] = "Generic";
public:
  bool setCursor(int x, int y);
  bool setCursorMode(CursorMode mode);
  bool setFont(Unicode::Font *font);
  void print(char *str, Color fgColor, Color bgColor);
  void clear();
  bool start();
  bool stop();
  VGATextDisplay(int width, int height);
};

#endif // VGATEXTDISPLAY_H

// LENS operating system kernel copyright (C) 2015 Alexander Logan Martin. All rights reserved.
#include <types.h>
#include <drivers/drivers.h>
#include <util/util.h>
#include <kernel.h>

/**
 * @file util/strutil.cpp
 *
 * String utilitie functions.
 */

template <class T> void swap(T& a, T& b) {
  T c(a);
  a=b;
  b=c;
}

void strReverse(char str[], size_t length) {
    size_t start = 0;
    size_t end = length -1;
    while (start < end)
    {
        swap(*(str+start), *(str+end));
        start++;
        end--;
    }
}

char* itoa(char* str, int base, long num) {
    int i = 0;
    bool isNegative = false;

    // Handle 0 explicitly, otherwise empty string is printed for 0.
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }

    // Process individual digits.
    while (num != 0)
    {
        long rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'.
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator.

    // Reverse the string.
    strReverse(str, i);

    return str;
}

void printInt(long num, int base) {
  char str[64];
  itoa(str, base, num);
  printk(&str[0]);
}

size_t strlen(const char *str) {
  size_t len = 0;
  for (; str[len] != 0; len++) {}
  return len;
}

// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sync.h"

/**
 * @file sync/sync.cpp
 *
 * Synchronization utilities (locks etc.).
 */

bool acquireLock(Lock *lock, int tries) {
  // TODO: Disable interrupts.
  for (int i = 0; i <= tries; i++) {
    if (lock->locked) {
      continue;
    } else {
      lock->locked = 1;
      // TODO: Reenable interrupts.
      return true;
    }
  }
  // TODO: Reenable interrupts.
  return false;
}

void releaseLock(Lock *lock) {
  lock->locked = 0;
}

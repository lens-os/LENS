// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PS2CONTROLLER_H
#define PS2CONTROLLER_H

#include <types.h>
#include <io/IORange.h>
#include <drivers/abstract/Device.h>

#define DRIVER_PS2_CONTROLLER // Tells other components that a PS/2 controller driver is available.

/**
 * A PS/2 controller.
 *
 * Manages PS/2 devices as children.
 */
class PS2Controller : Device { // The PS/2 controller itself.
private:
  IORange range;
  bool singleChannel;
  void outB(uint16_t offset, char data);
  bool outputBufferIsEmpty();
  bool inputBufferIsEmpty();
  void flushOutputBuffer();
protected:
  char generalType[16] = "PS2Controller";
  char specificType[16] = "Generic";
public:
  bool sendByteToDevice(char toSend, int device);
  char getByteFromDevice(int device);
  void resetSystem();
  bool start();
  bool stop();
  PS2Controller();
};

#endif // PS2CONTROLLER_H

// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <kexcept/Exception.h>

#ifndef MEMORYEXCEPTION_H
#define MEMORYEXCEPTION_H

/**
 * @file MemoryException.h
 *
 * Memory-management exceptions and error codes.
 */

#define OP_ALLOC_FRAMES 1 ///< allocateFrames call.
#define OP_ADD_FRAMES 2 ///< addFrames call.

#define ERR_OUT_OF_MEMORY 1 ///< Either there is no memory available or the memory manager was never initialized.
#define ERR_BLOCKS_TOO_SMALL 2 ///< There is no contiguous block large enough to satisfy the request.
#define ERR_NO_LOCK 3 ///< Failed to acquire the memory management lock.

/**
 * A memory-management exception.
 */
class MemoryException : public KExcept::Exception {
public:
  int code; ///< The numeric exception code. See MemoryException.h
  int operation; ///< The operation code. See MemoryException.h
  size_t numFrames; ///< The number of frames in the block.
  void *block; ///< The block involved.
  void asString(char *buf);
  MemoryException(int code, int operation, size_t numFrames, void *block);
};

#endif // MEMORYEXCEPTION_H

// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <util/strutil.h>
#include "MessageException.h"

using namespace KExcept;

void MessageException::asString(char *buf) {
  sprintf(buf, "MessageException (code %d): %s", this->code, this->message);
}

MessageException::MessageException(char *message, int code): message(message), code(code) {}
MessageException::MessageException(): message("Unknown"), code(0) {}

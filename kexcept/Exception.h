// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <types.h>

namespace KExcept {

/**
 * An exception that can be thrown and caught with throwExc and catchCall.
 *
 * Contains a numeric exception code and has a method to convert it into a string for error reporting.
 */
class Exception { // Base class for kexcept exceptions.
public:
  int code; ///< The numeric exception code. Defined by the throwing code.
  virtual void asString(char *buf) = 0;
  Exception(int code);
  Exception();
};

}

#endif // EXCEPTION_H

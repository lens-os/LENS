// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>

/**
 * @file sync/sync.h
 *
 * Synchronization utilities (locks etc.).
 */

typedef struct Lock { // Very basic psudo-spinlock.
  volatile int locked;
} Lock;

#define LOCK_INIT {0}

bool acquireLock(Lock *lock, int tries);
void releaseLock(Lock *lock);

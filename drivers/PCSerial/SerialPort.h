// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>
#include <drivers/abstract/Device.h>
#include <io/IORange.h>

#ifndef SERIALPORT_H
#define SERIALPORT_H
#define DRIVER_SERIALPORT

typedef enum Baud {
  rate_115200 = 1,
  rate_57600 = 2,
  rate_38400 = 3,
  rate_28800 = 4,
  rate_19200 = 6,
  rate_14400 = 8,
  rate_12800 = 9,
  rate_11520 = 10,
  rate_9600 = 12,
  rate_7200 = 16,
  rate_6400 = 18,
} Baud;

#define DATA_5 0x00 // xxxxxx00
#define DATA_6 0x01 // xxxxxx01
#define DATA_7 0x02 // xxxxxx10
#define DATA_8 0x03 // xxxxxx11

#define STOP_1 0x00 // xxxxx0xx
#define STOP_2 0x04 // xxxxx1xx

#define PARITY_NONE 0x00 // xx000xxx
#define PARITY_ODD 0x08 // xx001xxx
#define PARITY_EVEN 0x18 // xx011xxx
#define PARITY_MARK 0x28 // xx101xxx
#define PARITY_SPACE 0x38 // xx111xxx

/**
 * A PC RS-232 serial port.
 *
 * Requires some configuration to be used.
 */
class SerialPort : Device {
private:
  int portNumber;
  Baud baud;
  char encoding;
  IORange range;
  bool valid;
  bool up;
  bool setup();
protected:
  char generalType[16] = "SerialPort";
  char specificType[16] = "PCSerialPort";
public:
  bool write(char *data, size_t len);
  bool read(char *buf);
  bool readMultiple(char *buf, size_t len);
  bool setBaud(Baud rate);
  bool setEncoding(char data, char stop, char parity);
  bool start();
  bool stop();
  SerialPort(int portNumber);
  SerialPort();
};

#endif

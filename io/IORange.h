// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>

#ifndef IO_H
#define IO_H

extern bool debugIO;

/**
 * A range of I/O ports/addresses.
 *
 * Provides methods to perform input and output relative to the range.
 */
class IORange {
private:
  uint16_t base;
public:
  char inB(uint16_t offset);
  void outB(uint16_t offset, char data);
  uint16_t inW(uint16_t offset);
  void outW(uint16_t offset, uint16_t data);
  uint32_t inL(uint16_t offset);
  void outL(uint16_t offset, uint32_t data);
  void ioWait();
  IORange(uint16_t basePort);
  IORange();
};

#endif // IO_H

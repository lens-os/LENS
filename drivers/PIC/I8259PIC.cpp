// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "I8259PIC.h"
#define DEBUG true
#include <debug.h>
#include <util/util.h>

#define PICINIT 0x10
#define PICINIT_ENVIRON 0x01

#define ENVIRON_8086 0x01

#define CASCADE_MASTER 0x04
#define CASCADE_SLAVE 0x02

bool I8259PIC::mapIRQs(int base) {
  char masterMask = this->master.inB(1);
  char slaveMask = this->slave.inB(1);
  
  this->master.outB(0, PICINIT | PICINIT_ENVIRON);
  this->slave.outB(0, PICINIT | PICINIT_ENVIRON);
  this->master.ioWait();

  this->master.outB(1, (char) base);
  this->slave.outB(1, (char) base + 8);
  this->master.ioWait();

  this->master.outB(1, CASCADE_MASTER);
  this->slave.outB(1, CASCADE_SLAVE);
  this->master.ioWait();

  this->master.outB(1, ENVIRON_8086);
  this->slave.outB(1, ENVIRON_8086);
  this->master.ioWait();

  this->master.outB(1, masterMask);
  this->slave.outB(1, slaveMask);

  return true;
}

bool I8259PIC::maskIRQ(int index, bool enabled) {
  IORange *controller = &this->master;
  if (index > 7) {
    controller = &this->slave;
    index -= 8;
  }

  char mask = controller->inB(1);
  char diff = 1 << index;

  if (enabled) {
    diff = ~diff;
    mask = mask & diff;
  } else {
    mask = mask | diff;
  }

  controller->outB(1, mask);

  return true;
}

void I8259PIC::maskAllIRQs(bool enabled) {
  char mask = enabled ? 0x00 : 0xFF;
  this->master.outB(1, mask);
  this->slave.outB(1, mask);
}

bool I8259PIC::getIRQMasked(int index) {
  IORange *controller = &this->master;
  if (index > 7) {
    controller = &this->slave;
    index -= 8;
  }

  char mask = ~(1 << index);

  return !(controller->inB(1) & mask);
}

void I8259PIC::endOfInterrupt(int index) {
  if (index >= 8) {
    this->slave.outB(0, 0x20);
  }
  this->master.outB(0, 0x20);
}

bool I8259PIC::checkSpuriousInterrupt(int index) {
  char isr;
  if (index == 7) {
    this->master.outB(0, 0x0b);
    isr = this->master.inB(0);
  } else if (index == 15) {
    this->slave.outB(0, 0x0b);
    isr = this->slave.inB(0);
  } else {
    return false;
  }

  if (isr & 0x80) { // If the high bit is set, this is a real interrupt.
    return false;
  } else {
    this->master.outB(0, 0x20); // Need to send EOI to the master so it doesn't get confused.
    return true;
  }
}

bool I8259PIC::checkInterruptValid(int index) {
  return index != 2;
}

bool I8259PIC::start() {
  this->maskAllIRQs(false); // Disable all IRQs by default.
  this->mapIRQs(0xEF); // Map IRQs to highest possible position by default.
  return true;
}

bool I8259PIC::stop() {
  this->maskAllIRQs(false);
  return true;
}

I8259PIC::I8259PIC() {
  this->master = IORange(0x20);
  this->slave = IORange(0xA0);
}

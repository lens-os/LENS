# Building guide

## Prerequisites

You must have the following software available:

* GNU `make` or compatible.
* A recent GCC cross-compiler and GNU cross-binutils for target `x86_64-elf`. [This](https://wiki.osdev.org/GCC_Cross-Compiler) guide on osdev.org may be helpful.
* `nasm`.

All of this software must be in your `$PATH`.

## Building

Building LENS should be fairly simple, as long as everything is set up correctly:

1. Change to the root directory of LENS.
2. Run `make`.
3. Files named `lenskernel` and `lenskernel.debug` should be generated. These are ELF files containing, respectively, the kernel and its debug information.

## Troubleshooting

The first thing to try if something goes wrong is to `make clean` before `make`ing again. Make sure you are in the correct directory and that your `$PATH` is set correctly. Consider upgrading your compiler, binutils, and `nasm`.

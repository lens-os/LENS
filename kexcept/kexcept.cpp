// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kexcept.h"
#include <util/util.h>

namespace KExcept {

State currentState = {NULL, NULL};

/**
 * Calls a function while catching any exception it throws.
 *
 * @param func The function to call.
 * @param param The parameter to pass to the function.
 *
 * @return A structure containing the result or the caught exception.
 */
CatchResult catchCall(catchableFunction func, void *param) {
  ThreadState state;
  ThreadState *savedState = currentState.currentCatchState;
  CatchResult result;
  if (saveState(&state)) {
    result.caught = currentState.currentException;
    result.result = NULL;
  } else {
    currentState.currentCatchState = &state;
    result.caught = NULL;
    result.result = func(param);
  }
  currentState.currentCatchState = savedState;
  return result;
}

/**
 * Throws an exception.
 *
 * !!BIG WARNING!!
 *
 * throwExc() *does not* unwind the stack; as such, it doesn't call any destructors or release any memory!
 * If you call a function that throws an exception and you have objects that must be destroyed or memory that must be released, you *must* set a catch point to do this yourself!
 * To facilitate this, please note functions that throw an exception, or that call throwing functions without catching in their documentation.
 *
 * @param exc The exception to throw.
 */
void throwExc(Exception *exc) {
  currentState.currentException = exc;
  if (currentState.currentCatchState != NULL) {
    restoreState(currentState.currentCatchState);
  } else {
    char buf[256] = "Uncaught ";
    exc->asString(&buf[9]);
    panic(&buf[0]);
  }
}

}

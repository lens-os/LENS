// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <types.h>

#ifndef INTERRUPTMGR_H
#define INTERRUPTMGR_H

#define INTDESC_TYPE_INTERRUPT 0x0E // Interrupts disabled.
#define INTDESC_TYPE_TRAP 0x0F // Interrupts remain enabled.
#define INTDESC_PRIV_KERNEL 0x00
#define INTDESC_PRIV_USER 0x60 // Allows INTing from userspace (system calls etc.).
#define INTDESC_PRESENT 0x80

namespace InterruptManager {

/**
 * @namespace InterruptManager
 *
 * The interrupt manager.
 *
 * Manages the Interrupt Descriptor Table and interrupt enablement, *not* the interrupt controllers.
 */

typedef struct InterruptDescriptor {
  uint16_t offset1; ///< The lowest 16 bits of the address.
  uint16_t selector; ///< The GDT code selector.
  uint8_t stack; ///< The interrupt stack index.
  uint8_t flags; ///< The type and attributes of the descriptor.
  uint16_t offset2; ///< The mid 16 bits of the address.
  uint32_t offset3; ///< The high 32 bits of the address.
  uint32_t zero;
} __attribute__((__packed__)) InterruptDescriptor;

typedef struct IDTDescriptor {
  uint16_t size; // One less than actual size.
  InterruptDescriptor *IDT;
} __attribute__((__packed__)) IDTDescriptor;

struct interruptFrame {
  uint64_t rip;
  uint64_t cs;
  uint64_t rflags;
  uint64_t rsp;
  uint64_t ss;
};

typedef void (*InterruptHandler)(interruptFrame *); // An interrupt handler.
typedef void (*ExceptionHandler)(interruptFrame *, uint64_t); // An exception handler.

typedef union ISR {
  InterruptHandler intr;
  ExceptionHandler excp;
  void *ptr;
} ISR;

bool loadIDT(IDTDescriptor *IDTDesc);
bool setInterruptHandler(int index, ISR handler, bool disableDuring, bool user, int stack);
ISR getInterruptHandler(int index);
bool removeInterruptHandler(int index);
bool getLocalInterruptsEnabled();
void setLocalInterruptsEnabled(bool enabled);
void init();

}

#endif // INTERRUPTMGR_H

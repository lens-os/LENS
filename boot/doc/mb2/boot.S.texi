/* @r{ boot.S - bootstrap the kernel} */
/* @r{ Copyright (C) 1999, 2001, 2010  Free Software Foundation, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
} */

#define ASM_FILE        1
#include <multiboot2.h>

/* @r{ C symbol format. HAVE_ASM_USCORE is defined by configure.} */
#ifdef HAVE_ASM_USCORE
# define EXT_C(sym)                     _ ## sym
#else
# define EXT_C(sym)                     sym
#endif

/* @r{ The size of our stack (16KB).} */
#define STACK_SIZE                      0x4000

/* @r{ The flags for the Multiboot header.} */
#ifdef __ELF__
# define AOUT_KLUDGE 0
#else
# define AOUT_KLUDGE MULTIBOOT_AOUT_KLUDGE
#endif
        
        .text

        .globl  start, _start
start:
_start:
        jmp     multiboot_entry

        /* @r{ Align 64 bits boundary.} */
        .align  8
        
        /* @r{ Multiboot header.} */
multiboot_header:
        /* @r{ magic} */
        .long   MULTIBOOT2_HEADER_MAGIC
        /* @r{ ISA: i386} */
        .long   GRUB_MULTIBOOT_ARCHITECTURE_I386
        /* @r{ Header length.} */
        .long   multiboot_header_end - multiboot_header
        /* @r{ checksum} */
        .long   -(MULTIBOOT2_HEADER_MAGIC + GRUB_MULTIBOOT_ARCHITECTURE_I386 + (multiboot_header_end - multiboot_header))
#ifndef __ELF__
address_tag_start:      
        .short MULTIBOOT_HEADER_TAG_ADDRESS
        .short MULTIBOOT_HEADER_TAG_OPTIONAL
        .long address_tag_end - address_tag_start
        /* @r{ header_addr} */
        .long   multiboot_header
        /* @r{ load_addr} */
        .long   _start
        /* @r{ load_end_addr} */
        .long   _edata
        /* @r{ bss_end_addr} */
        .long   _end
address_tag_end:
entry_address_tag_start:        
        .short MULTIBOOT_HEADER_TAG_ENTRY_ADDRESS
        .short MULTIBOOT_HEADER_TAG_OPTIONAL
        .long entry_address_tag_end - entry_address_tag_start
        /* @r{ entry_addr} */
        .long multiboot_entry
entry_address_tag_end:
#endif /* @r{ __ELF__} */
framebuffer_tag_start:  
        .short MULTIBOOT_HEADER_TAG_FRAMEBUFFER
        .short MULTIBOOT_HEADER_TAG_OPTIONAL
        .long framebuffer_tag_end - framebuffer_tag_start
        .long 1024
        .long 768
        .long 32
framebuffer_tag_end:
        .short MULTIBOOT_HEADER_TAG_END
        .short 0
        .long 8
multiboot_header_end:
multiboot_entry:
        /* @r{ Initialize the stack pointer.} */
        movl    $(stack + STACK_SIZE), %esp

        /* @r{ Reset EFLAGS.} */
        pushl   $0
        popf

        /* @r{ Push the pointer to the Multiboot information structure.} */
        pushl   %ebx
        /* @r{ Push the magic value.} */
        pushl   %eax

        /* @r{ Now enter the C main function...} */
        call    EXT_C(cmain)

        /* @r{ Halt.} */
        pushl   $halt_message
        call    EXT_C(printf)
        
loop:   hlt
        jmp     loop

halt_message:
        .asciz  "Halted."

        /* @r{ Our stack area.} */
        .comm   stack, STACK_SIZE

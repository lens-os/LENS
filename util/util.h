// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UTIL_H
#define UTIL_H

#include <types.h>

/**
 * @file util/util.h
 *
 * Various utility functions used across the kernel.
 */

#define UNUSED_PARAM(param) {(param) = (param);}

bool isBitSet(char toCheck, int bit);
int countBitsSet(char toCheck);
void memcpy(void *source, void *dest, size_t length);
void printk(const char *message);
void halt();
void panic(const char *reason);
void printVersion();

#endif // UTIL_H

// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <drivers/abstract/Device.h>
#include <types.h>

#ifndef INTERRUPTCONTROLLER_H
#define INTERRUPTCONTROLLER_H

/**
 * An abstract interrupt controller (e.g. i8259 PIC, APIC).
 *
 * Manages interrupt masking, mapping, end of interrupt signals, and so forth.
 */
class InterruptController : public Device {
protected:
  char generalType[16] = "IntController";
  char specificType[16] = "Unknown";
public:
  int numIRQs; ///< The number of IRQs that the controller provides.
  /**
   * Maps the IRQs to local interrupts.
   *
   * @param base The first interrupt vector to map at.
   *
   * @return Whether mapping at base is possible.
   */
  virtual bool mapIRQs(int base) = 0;
  /**
   * Sets whether the specified IRQ is routed to the CPU.
   *
   * @param index The IRQ to mask.
   * @param enabled Whether the IRQ should be enabled.
   *
   * @return The previous status.
   */
  virtual bool maskIRQ(int index, bool enabled) = 0;
  /**
   * Masks every IRQ.
   *
   * @param enabled Whether to enable the IRQs.
   */
  virtual void maskAllIRQs(bool enabled) = 0;
  /**
   * Gets whether the specified IRQ is enabled.
   *
   * @param index The IRQ to check.
   *
   * @return Whether the IRQ is enabled.
   */
  virtual bool getIRQMasked(int index) = 0;
  /**
   * Must be called at the end of handling an IRQ.
   *
   * @param index The interrupt handled.
   */
  virtual void endOfInterrupt(int index) = 0;
  /**
   * Must be called at the beginning of IRQ handling to check if the IRQ is spurious.
   *
   * @param index The interrupt being handled.
   *
   * @return Whether the IRQ's normal handling should run. If false, endOfInterrupt should *not* be called.
   */
  virtual bool checkSpuriousInterrupt(int index) = 0;
  /**
   * Checks if an IRQ is actually valid for external use (e.g. 8259 cascading uses IRQ 2).
   *
   * @param index The interrupt to check
   * @return Whether the interrupt is legal.
   */
  virtual bool checkInterruptValid(int index) = 0;
};

#endif // INTERRUPTCONTROLLER_H

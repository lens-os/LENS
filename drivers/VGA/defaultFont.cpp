// LENS operating system kernel copyright (C) 2018 Alexander Martin.
/*
This file is part of LENS.

LENS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LENS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LENS.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "defaultFont.h"
#include <unicode/types.h>

using namespace Unicode;

/**
 * Creates the default VGA CP437/Unicode mapping font.
 *
 * @return The created font.
 */
Font *getDefaultFont() { // Gets the default VGA font mappings. Currently has no glyphs, only mappings.
  Font *font = new Font;
  font->numGlyphs = 0;
  GlyphAssoc *assoc = new GlyphAssoc;

  assoc->firstPoint = 0x0020; // Map the printable subset of the Basic Latin block, which is contiguous in both Unicode and CP437.
  assoc->firstGlyph = 0x20;
  assoc->numAssocs = 0x5E;
  assoc->next = new GlyphAssoc;
  font->firstGlyphAssoc = assoc;

  assoc = assoc->next;
  assoc->firstPoint = 0xFFFD; // Map the Replacement Character to a question mark.
  assoc->firstGlyph = '?';
  assoc->numAssocs = 1;
  return font;
}

# LENS
LENS (Lightweight Extensible Next-gen System) is an open-source hobbyist operating system, developed partially as an experiment.

## License

LENS operating system kernel copyright (C) 2018 Alexander Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## Building

See [BUILDING.md](BUILDING.md)

## Code style

See [STYLE.md](STYLE.md)

## Directory tree

The LENS source tree is structured like so:

```
/ - Some misc kernel sources, Makefile, scripts, information.
├─ /boot - Stuff about Multiboot. Contents copyrighted by the Free Software Foundation.
├─ /drivers - Drivers.
│  ├─ abstract - Abstract drivers will go here.
│  ├─ PCSerial - PC serial port driver.
│  ├─ PS2 - PS/2 drivers (controller and keyboard).
│  └─ VGA - VGA-related drivers.
├─ /interrupts - Interrupt management.
├─ /io - Basic I/O functions/classes.
├─ /kexcept - A simple exception-handling system that avoids stack unwinding.
├─ /mem - Memory management.
├─ /printf - A printf/sprintf library.
├─ /sync - Synchronization (aka locking).
├─ /unicode - Unicode stuff (UTF-8 and console fonts).
└─ /util - Various utility functions.
```

; LENS operating system kernel copyright (C) 2018 Alexander Martin.

; This file is part of LENS.

; LENS is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; LENS is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with LENS.  If not, see <http://www.gnu.org/licenses/>.

global saveState

saveState: ; Stores all the registers.

  mov rax, rdi ; Get parameter (SysV x86-64 puts params in registers).

  mov [rax], rbx
  add rax, 8
  mov [rax], rcx
  add rax, 8
  mov [rax], rdx
  add rax, 8

  mov [rax], rsp
  add rax, 8
  mov [rax], rbp
  add rax, 8

  mov [rax], rsi
  add rax, 8
  mov [rax], rdi
  add rax, 8

  mov [rax], r8
  add rax, 8
  mov [rax], r9
  add rax, 8
  mov [rax], r10
  add rax, 8
  mov [rax], r11
  add rax, 8
  mov [rax], r12
  add rax, 8
  mov [rax], r13
  add rax, 8
  mov [rax], r14
  add rax, 8
  mov [rax], r15

  push rbx ; Save our intermediate register.
  add rax, 8
  mov rbx, [rsp+8]
  mov [rax], rbx ; Save the return address.
  pop rbx ; Restore.

  mov rax, 0 ; 0 = not restored.
  ret

global restoreState

restoreState: ; Restores all the registers, and the stack, and returns to saveState's caller. Clobbers all local state.
  mov rax, rdi ; Get parameter.

  mov rbx, [rax] ; Basic GPRs.
  add rax, 8
  mov rcx, [rax]
  add rax, 8
  mov rdx, [rax]
  add rax, 8

  mov rsp, [rax] ; Stack.
  add rax, 8
  mov rbp, [rax]
  add rax, 8

  mov rsi, [rax] ; String copy registers.
  add rax, 8
  mov rdi, [rax]
  add rax, 8

  mov r8, [rax] ; Extended GPRs.
  add rax, 8
  mov r9, [rax]
  add rax, 8
  mov r10, [rax]
  add rax, 8
  mov r11, [rax]
  add rax, 8
  mov r12, [rax]
  add rax, 8
  mov r13, [rax]
  add rax, 8
  mov r14, [rax]
  add rax, 8
  mov r15, [rax]

  push rbx ; Save our intermediate register.
  add rax, 8
  mov rbx, [rax]
  mov [rsp+8], rbx ; Rewrite the return address.
  pop rbx ; Restore

  mov rax, 1 ; With the stack moved, we are returning to saveState()'s caller. 1 = restored.
  ret
